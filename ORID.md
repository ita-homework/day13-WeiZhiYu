# O

- Promise: The Promise object represents the final completion (or failure) of the asynchronous operation and its resulting value. A Promise is in one of these states: pending, fulfilled (resolved) and rejected. Use Promises to handle asynchronous operations so that web pages are not blocked by requests and other operations.
- React Router: Today we learn how to use react router to jump web pages. We have written four pages: Homepage, Help page, Done list page and error page.
- Axios: Today we use axios to integrate front-end and back-end, with mockAPI, making the code cleaner.
- Ant Design: Antd is an UI library, it contains a set of high quality components and demos for building rich, interactive user interfaces. In today's homework, I try to use Antd to make web page responsive and beautiful.

# R

I am frustrated.

# I

Using antd, I was able to make a beautiful UI relatively quickly, but I don't know how to make page responsive, I have relatively little experience in this field.

# D

I will search online to figure how to make page responsive.