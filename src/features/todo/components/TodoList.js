import TodoGenerator from "./TodoGenerator";
import TodoGroup from "./TodoGroup";
import { useEffect } from "react";
import { useTodos } from "../../hooks/useTodo";

export default function TodoList() {
    const { loadTodos } = useTodos()
    useEffect(() => { loadTodos() })
    return (
        <div className='todo-list'>
            <h1>Todo List</h1>
            <TodoGroup />
            <TodoGenerator />
        </div>
    );
}