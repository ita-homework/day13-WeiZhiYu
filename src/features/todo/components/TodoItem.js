import { useTodos } from "../../hooks/useTodo";
import { useState } from 'react';
import { EditTwoTone, DeleteTwoTone } from '@ant-design/icons';
import TodoItemModal from "./TodoItemModal";

export default function TodoItem({ task }) {
    const [isModalOpen, setIsModalOpen] = useState(false)
    const { updateTodo, deleteTodo } = useTodos()

    const handleTaskNameClick = async () => {
        await updateTodo(task.id, { done: !task.done })
    }

    const hanlleRemoveButtonClick = async () => {
        if (window.confirm('Are you sure you wish to delete this item?')) {
            await deleteTodo(task.id)
        }
    }

    return (
        <div className='todo-item'>
            <div className={`task-name ${task.done ? 'done' : ''}`}
                onClick={handleTaskNameClick}>
                {task.name}
            </div>
            <div className="todo-item-buttons">
                <EditTwoTone className="editButton" onClick={() => { setIsModalOpen(true) }} />
                <DeleteTwoTone onClick={hanlleRemoveButtonClick} />
            </div>
            <TodoItemModal taskId={task.id} isModalOpen={isModalOpen} setIsModalOpen={setIsModalOpen} />
        </div>
    );
}