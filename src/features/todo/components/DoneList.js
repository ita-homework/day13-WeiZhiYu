import DoneGroup from './DoneGroup.js';
export default function DoneList() {
    return (
        <div className='done-list'>
            <h1>Done List</h1>
            <DoneGroup />
        </div>
    );
}