import { Form, Modal } from 'antd';
import TodoItemForm from './TodoItemForm';

export default function TodoItemModal(props) {
    const [form] = Form.useForm();
    const { taskId, isModalOpen, setIsModalOpen } = props

    const handleOk = () => {
        setIsModalOpen(false)
        form.submit()
    }

    const handleCancel = () => {
        setIsModalOpen(false);
    };

    return (
        <Modal title="Change Task Name" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
            <TodoItemForm taskId={taskId} form={form} />
        </Modal>
    )
}