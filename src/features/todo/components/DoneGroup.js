import DoneItem from "./DoneItem";
import { useSelector } from 'react-redux'

export default function DoneGroup() {
    const doneTasks = useSelector(state => state.todo.tasks).filter(task => task.done)
    return (
        <div className='done-group'>
            {doneTasks.map(((doneTask) =>
                <DoneItem key={doneTask.id} task={doneTask}></DoneItem>
            ))}
        </div>
    );
}