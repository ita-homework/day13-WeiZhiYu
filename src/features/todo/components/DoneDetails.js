import { useParams } from "react-router-dom"
import { useSelector } from "react-redux"

export default function DoneDetails() {
  const { id } = useParams()
  const doneTask = useSelector(state => state.todo.tasks).find(task => task.id === id)

  return (
    <div className="done-detail">
      <h1>Done Details</h1>
      <div>{doneTask?.id}</div>
      <div>{doneTask?.name}</div>
    </div>
  );
}