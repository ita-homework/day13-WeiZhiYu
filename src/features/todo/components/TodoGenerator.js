import { useState } from "react";
import { useTodos } from "../../hooks/useTodo";


export default function TodoGenerator() {
    const { createTodo } = useTodos()

    const [taskName, setTaskName] = useState("");

    const handleTaskNameChange = (e) => {
        const value = e.target.value;
        setTaskName(value);
    }

    const handleAddTodoTask = async () => {
        await createTodo({name: taskName})
        setTaskName("");
    }
    return (
        <div className='todo-generator'>
            <input placeholder='input a new todo here...' onChange={handleTaskNameChange} value={taskName}></input>
            <button onClick={handleAddTodoTask} disabled={!taskName} > Add </button>
        </div>
    );
}