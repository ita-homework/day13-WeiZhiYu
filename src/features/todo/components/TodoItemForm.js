import { Form, Input } from 'antd';
import { useTodos } from "../../hooks/useTodo";
import React from 'react';

export default function TodoItemForm({ taskId, form }) {
    const { updateTodo } = useTodos()
    const onFinish = async (values) => {
        const newTaskName = values.taskName
        await updateTodo(taskId, { name: newTaskName })
        form.resetFields()
    }
    return (
        <Form
            form={form}
            name={`Task${taskId}`}
            onFinish={onFinish}
            style={{
                maxWidth: 600,
            }}
        >
            <Form.Item name="taskName" rules={[{ required: true, },]} >
                <Input />
            </Form.Item>
        </Form>
    )
}