import { useNavigate } from "react-router-dom";
export default function TodoItem(props) {
    const navigate = useNavigate();
    const handleTaskNameClick = () => {
        navigate('/done/' + props.task.id)
    }
    return (
        <div className='done-item'>
            <div className={`task-name ${props.task.done ? 'done' : ''}`}
                onClick={handleTaskNameClick}>{props.task.name}
            </div>
        </div>
    );
}