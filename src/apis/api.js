import axios from "axios";

const api = axios.create({
    baseURL: "https://64c0b6870d8e251fd11265e4.mockapi.io",
})

export default api