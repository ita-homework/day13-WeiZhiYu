import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import store from './store';
import { Provider } from 'react-redux';
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import ErrorPage from './pages/ErrorPage.js'
import HelpPage from './pages/HelpPage.js'
import DoneList from './features/todo/components/DoneList.js';
import TodoList from './features/todo/components/TodoList.js';
import DoneDetails from './features/todo/components/DoneDetails.js';

const root = ReactDOM.createRoot(document.getElementById('root'));

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    errorElement: <ErrorPage />,
    children: [
      {
        index: true,
        element: <TodoList />,
      }
      ,
      {
        path: "/done",
        element: <DoneList />,
      },
      {
        path: "/done/:id",
        element: <DoneDetails />,
      },
      {
        path: "/help",
        element: <HelpPage />,
      },
    ]
  }
])

root.render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
  </React.StrictMode>
);
