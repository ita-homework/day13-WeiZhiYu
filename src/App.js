import './App.css';
import { NavLink, Outlet } from 'react-router-dom';
import { Divider, Space } from 'antd';


function App() {
  return (
    <div className="app">
      <Space split={<Divider type="vertical" />}>
        <NavLink to={'/'}>Home</NavLink>
        <NavLink to={'/done'}>Done List</NavLink>
        <NavLink to={'/help'}>Help</NavLink>
      </Space>
      <Outlet></Outlet>
    </div>
  );
}
export default App;
